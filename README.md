
Just some tiny scripts to work with FLAC files.

To be installed in /usr/local/bin, or wherever it suits you. It requires
a few external tools that can be obtained by installing the following Debian 
packages:

    apt-get install cuetools flac shntool

